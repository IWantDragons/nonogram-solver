# The board itself. We should have a 2D-list for the board, and
# two lists for column and row encodings


class boardClass:
    board = [[]]
    # Encodings are an array of strings, at the moment. It would probably be better to have
    # encodings as a list or dict of lists, but that'll be for later
    rowEncodings = []
    columnEncodings = []
    rowAmount = 0
    columnAmount = 0

    def __init__(self, rowNum, colNum, rowList, colList):
        # 0-initialize board.
        self.board = [[0 for i in range(0, colNum)] for i in range(0, rowNum)]
        self.rowEncodings = rowList
        self.columnEncodings = colList
        self.rowAmount = rowNum
        self.columnAmount = colNum

    def __eq__(self, other):
        if not isinstance(other, boardClass):
            return False
        return (
            self.board == other.board
            # and self.rowEncodings == other.rowEncodings
            # and self.columnEncodings == other.columnEncodings
        )

    def get_board(self):
        return self.board

    def set_board(self, board):
        self.board = board

    def get_row_encodings(self):
        return self.rowEncodings

    def set_row_encodings(self, rows):
        self.rowEncodings = rows

    def get_column_encodings(self):
        return self.columnEncodings

    def set_column_encodings(self, columns):
        self.columnEncodings = columns

    def get_row_number(self, index):
        return self.board[index]

    def get_col_number(self, index):
        return [self.board[i][index] for i in range(0, len(self.board))]

    def get_cell(self, row, col):
        return self.board[row][col]

    def set_cell(self, row, col, value):
        self.board[row][col] = value

    def set_cell_sequence(self, coordStart, coordEnd, value):
        for i in range(coordStart[0], coordEnd[0]):
            for j in range(coordStart[1], coordEnd[1]):
                self.set_cell(i, j, value)

    # Prints row-based
    # TODO: Prettify the output a bit (use special characters for blank/filled)
    def print_board(self):
        for row in range(0, len(self.board)):
            for col in range(0, len(self.board[row])):
                print(str(self.board[row][col]) + "  ", end="")
            print()  # Print a newline when a row is done

    def print_game(self):
        # TODO: Prettify the output a bit
        # (use special characters for blank/filled)
        
        # Longest column and row found by string length
        longestColumn = max(len(x) for x in self.columnEncodings)
        longestRow = max(len(x) for x in self.rowEncodings)

        # --- GENERATE ROW PRINT ---
        # Holding variable for each row line to print
        printRows = []
        # 3 chars pr. number in row encoding (double-digit + space)
        # ditto for number of columns
        # + 1 space between row numbers and board itself
        maxPrintRowLength = (longestRow * 3) + (self.columnAmount * 3) + 1
        # Go through each encoding
        for i in range(0, len(self.rowEncodings)):
            tempString = ""
            # For each number in the encoding
            for j in range(0, len(self.rowEncodings[i])):
                # Append the number, right-adjust to 3 characters
                appendNum = self.rowEncodings[i][j]
                tempString = tempString + str(appendNum).rjust(3)
            # Add the space between the board
            tempString = tempString + " "
            # Then add the board state for the given row
            for j in range(0, len(self.board[i])):
                tempString = tempString + '{:3}'.format(str(self.board[i][j]))
            # Right-adjust the whole row-string to the max length
            tempString = tempString.rjust(maxPrintRowLength)
            # Then append the row to the print, and continue
            printRows.append(tempString)

        # --- GENERATE COLUMNS PRINT ---
        # Holding variable for each level of column
        # The trick is just that we try to append element n in the column encoding
        # But if the number doesn't exist, we add a space instead.
        # This is built upside-down, but we flip it later when printing
        columnNumbers = []
        # For each n up to length of the longest/tallest column
        # Note! We start at 1, since we want to index the columns from the back
        # of the encoding, which should be the bottom when print
        for i in range(1, longestColumn + 1):
            # For each column
            for j in range(0, len(self.columnEncodings)):
                try:
                    # Get the number, left-adjusted to 3 characters
                    appendNum = self.columnEncodings[j][-i]
                    columnNumbers.append('{:<3}'.format(str(appendNum)))
                except IndexError:
                    # If the number doesn't exist, add spaces instead
                    columnNumbers.append("   ")

        # Spacing for the columns, as they're above the board
        # 3 for the space needed for a number (double-digit and 1 space)
        # 1 for the spacing between the row numbers and the actual board
        colSpacing = longestRow * 3 + 1
        printColumns = []
        # For each n up to the length of the longest/tallest column
        for i in range(0, longestColumn):
            tempString = ""
            # For each column
            for i in range(0, self.columnAmount):
                # Append the number (or spaces) for the given n heigh of the column
                tempString = tempString + columnNumbers.pop(0)
            # Right-adjust with the desired columnspacing
            printColumns.append(tempString.rjust(colSpacing + len(tempString)))
        # Once all column prints are gathered, flip it upside down
        printColumns = printColumns[::-1]

        print_lines = printColumns + printRows
        for line in print_lines:
            print(line)
